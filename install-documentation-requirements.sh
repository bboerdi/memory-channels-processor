#!/usr/bin/env bash

SD=$(cd "$(dirname "$0")"; pwd -P)
WD="$(pwd)"
SCRIPT=$(basename "$0")
SCRIPT_NAME=${SCRIPT%.*}
SCRIPT_EXTENSION=${SCRIPT##*.}
SELF=$SD/$SCRIPT

#/////////////////////////

if [ "$(whoami)" != "root" ] ; then
	echo "Sorry, you are not root"
	exit 1
fi

#/////////////////////////

APP_APT="$(command -v apt)"
APP_BUNDLE="$(command -v bundle)"
APP_CURL="$(command -v curl)"
APP_GPG="$(command -v gpg)"
APP_MKDIR="$(command -v mkdir)"
APP_TEE="$(command -v tee)"

#/////////////////////////

set -eu

#/////////////////////////

if [ ! -e "$APP_APT" ] ; then
    echo "The executable 'apt' wasn't found!"
    exit
fi

if [ ! -e "$APP_BUNDLE" ] ; then
    echo "The executable 'bundle' wasn't found!"
    exit
fi

if [ ! -e "$APP_CURL" ] ; then
    echo "The executable 'curl' wasn't found!"
    exit
fi

if [ ! -e "$APP_GPG" ] ; then
    echo "The executable 'gpg' wasn't found!"
    exit
fi

if [ ! -e "$APP_MKDIR" ] ; then
    echo "The executable 'mkdir' wasn't found!"
    exit
fi

if [ ! -e "$APP_TEE" ] ; then
    echo "The executable 'tee' wasn't found!"
    exit
fi

#/////////////////////////

cd "$SD/" || exit 1

#/////////////////////////

echo "--------------------"

echo "Install Ruby requirements"
su "${SUDO_USER:-$USER}" -c "$APP_BUNDLE config set path '.bundle/gems'"
su "${SUDO_USER:-$USER}" -c "$APP_BUNDLE install"

echo "--------------------"

echo "Install Node.js"

if [ ! -e "/etc/apt/keyrings/nodesource.gpg" ] ; then
	$APP_APT update
	$APP_APT install -y ca-certificates
	$APP_MKDIR -p /etc/apt/keyrings
	$APP_CURL -fsSL https://deb.nodesource.com/gpgkey/nodesource-repo.gpg.key | $APP_GPG --dearmor -o /etc/apt/keyrings/nodesource.gpg
fi


if [ ! -e "/etc/apt/sources.list.d/nodesource.list" ] ; then
	NODE_MAJOR=20
	echo "deb [signed-by=/etc/apt/keyrings/nodesource.gpg] https://deb.nodesource.com/node_$NODE_MAJOR.x nodistro main" | $APP_TEE /etc/apt/sources.list.d/nodesource.list
fi

APP_NODE="$(command -v node)"
if [ ! -e "$APP_NODE" ] ; then
	$APP_APT update
	$APP_APT install nodejs -y
fi

echo "--------------------"

APP_NODE="$(command -v node)"
APP_NPM="$(command -v npm)"
APP_NPX="$(command -v npx)"

echo "Install Antora"
# Install dependencies defined in 'package.json'
su "${SUDO_USER:-$USER}" -c "$APP_NPM install"
su "${SUDO_USER:-$USER}" -c "$APP_NPX antora -v"

echo "--------------------"
echo "Finished."
echo "--------------------"

#/////////////////////////

cd "$WD/" || exit 1

#/////////////////////////