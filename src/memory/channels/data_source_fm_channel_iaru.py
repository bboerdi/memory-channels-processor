import argparse

import pandas as pd
from pandas import DataFrame

from memory.channels import MemoryChannelProcessor
from memory.channels.data_sources import TextBaseSource
from memory.channels.processor import get_data_source_col_names, get_data_source_cols


class FMChannelIARUR1GeneratorSource(TextBaseSource):

    def setup_args(self, parser: argparse.ArgumentParser):
        pass

    def get_data(self, processor: MemoryChannelProcessor, args: argparse.Namespace) -> DataFrame:
        data = pd.DataFrame(columns=get_data_source_col_names())

        # Convert data types
        data = data.astype(get_data_source_cols())

        # -----------------------------------------

        step = 125
        rows = []

        # -- Generate data for 2m band ------------
        freq_2m = []

        # FM Simplex
        for freq in range(1452125, 1452875 + step, step):
            freq_2m.append(freq / 10000)
        # FM Simplex
        for freq in range(1453125, 1455875 + step, step):
            freq_2m.append(freq / 10000)

        for freq in freq_2m:
            channel_new = round((freq - 145) * 80)
            name_new = f"V%02d" % channel_new
            if (channel_new % 2) == 0:
                name_old = f"S%d" % (channel_new / 2)
            else:
                name_old = None

            if name_old is None:
                name = f"%s" % name_new
            else:
                name = f"%s (%s)" % (name_new, name_old)

            row = {
                'name': name,
                'band': "2m",
                'freq_tx': freq,
                'freq_rx': freq,
                'ctcss': False,
                'ctcss_tx': None,
                'ctcss_rx': None,
                'dmr': False,
                'dmr_id': None,
                'dstar': False,
                'dstar_rpt1': None,
                'dstar_rpt2': None,
                'fm': True,
                'landmark': None,
                'state': None,
                'country': None,
                'loc_exact': False,
                'lat': None,
                'long': None,
                'callsign': None
            }
            rows.append(row)

        # -- Generate data for 70cm band ----------
        freq_70cm = []

        # FM Simplex
        freq_70cm.append(4300125 / 10000)
        freq_70cm.append(4300375 / 10000)
        # FM Simplex
        for freq in range(4300625, 4303500 + step, step):
            freq_70cm.append(freq / 10000)
        # FM Simplex
        for freq in range(4330000, 4333875 + step, step):
            freq_70cm.append(freq / 10000)
        # FM Simplex
        for freq in range(4334125, 4335875 + step, step):
            freq_70cm.append(freq / 10000)

        for freq in freq_70cm:
            channel_new = round((freq - 430) * 80)
            name_new = f"U%03d" % channel_new
            if (channel_new >= 240) and (channel_new <= 288) and (channel_new % 2) == 0:
                name_old = f"SU%d" % ((channel_new - 240) / 2)
            else:
                name_old = None

            if name_old is None:
                name = f"%s" % name_new
            else:
                name = f"%s (%s)" % (name_new, name_old)

            row = {
                'name': name,
                'band': "70cm",
                'freq_tx': freq,
                'freq_rx': freq,
                'ctcss': False,
                'ctcss_tx': None,
                'ctcss_rx': None,
                'dmr': False,
                'dmr_id': None,
                'dstar': False,
                'dstar_rpt1': None,
                'dstar_rpt2': None,
                'fm': True,
                'landmark': None,
                'state': None,
                'country': None,
                'loc_exact': False,
                'lat': None,
                'long': None,
                'callsign': None
            }
            rows.append(row)
        # -----------------------------------------

        df_extended = pd.DataFrame(rows, columns=get_data_source_col_names())
        data = pd.concat([data, df_extended], ignore_index=True)

        # -----------------------------------------

        return self.process_data(data, processor, args)
