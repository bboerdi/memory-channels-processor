import argparse

import numpy as np
from pandas import DataFrame

from memory.channels.data_targets import Jinja2BaseTarget
from memory.channels.processor import is_empty

class ChirpTarget(Jinja2BaseTarget):

    def setup_args(self, parser: argparse.ArgumentParser):
        Jinja2BaseTarget.setup_args(self, parser)

    def prepare_data(self, data: DataFrame, args: argparse.Namespace) -> DataFrame:
        # Set values
        data.loc[data['offset'] > 0, 'dup'] = "-"
        data.loc[data['offset'] < 0, 'dup'] = "+"

        # Calculate
        data['tone'] = data.apply(self.get_tone, args=(args,), axis=1)
        data['chirp_comment'] = data.apply(self.get_comment, args=(args,), axis=1)

        return data

    def get_template_path(self, args: argparse.Namespace) -> str:
        return "chirp_next.csv.tpl"

    def get_tone(self, row, args: argparse.Namespace) -> str:
        result = ""
        if not is_empty(row['ctcss_rx']) and row['ctcss_rx'] > 0:
            result = "Tone"

        return result

    def get_comment(self, row, args: argparse.Namespace) -> str:
        return row['name'] + ' ' + row['band']
