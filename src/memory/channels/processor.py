"""
Memory Channels Processor
=================================

"""

# (c) 2023, OE3LRT <lukas@oe3lrt.at>

######################################################################

import argparse
import os
import re
import warnings
from abc import ABC, abstractmethod
from typing import AnyStr, Any, Iterable
from builtins import str

import numpy as np
import requests_cache

import pandas as pd
from pandas import DataFrame

from pyhamtools.locator import calculate_distance, calculate_heading, latlong_to_locator

import memory
from memory.channels import PY3_9_OR_LATER

FILTER_TYPE_FM = "fm"
FILTER_TYPE_DSTAR = "d-star"
FILTER_TYPE_DMR = "dmr"

ASCIIDOC_DATA_SOURCE_COLUMNS = "data_source_columns"
ASCIIDOC_DATA_TARGET_COLUMNS = "data_target_columns"
ASCIIDOC_NAME_FORMATS = "name_formats"

NAME_FORMAT_CALLSIGN = "callsign"
NAME_FORMAT_CALLSIGN_TEMPLATE = "{{ callsign | default('', True) }}"

NAME_FORMAT_CALLSIGN_5_CHAR = "5-char"
NAME_FORMAT_CALLSIGN_5_CHAR_TEMPLATE = "{{ callsign | default('', True) | removeprefix('OE') | substring(0,4) | ljust(4) }}{{ mode_short }}"

NAME_FORMAT_CALLSIGN_6_CHAR = "6-char"
NAME_FORMAT_CALLSIGN_6_CHAR_TEMPLATE = "{{ callsign | default('', True) | removeprefix('OE') | substring(0,4) | ljust(4) }}-{{ mode_short }}"

NAME_FORMAT_CALLSIGN_7_CHAR = "7-char"
NAME_FORMAT_CALLSIGN_7_CHAR_TEMPLATE = "{{ callsign | default('', True) | removeprefix('OE') | substring(0,5) | ljust(5) }}-{{ mode_short }}"

NAME_FORMAT_CALLSIGN_NAME = "callsign-name"
NAME_FORMAT_CALLSIGN_NAME_TEMPLATE = "{{ callsign | default('', True) | removeprefix('OE') | substring(0,4) | ljust(4) }}{{ '-' if name is defined and name | length }}{{ name | default('', True) | replaceumlauts }}"

NAME_FORMAT_CALLSIGN_MODE_NAME = "callsign-mode-name"
NAME_FORMAT_CALLSIGN_MODE_NAME_TEMPLATE = "{{ callsign | default('', True) | removeprefix('OE') | substring(0,4) | ljust(4) }}-{{ mode_short }}{{ '-' if name is defined and name | length }}{{ name | default('', True) | replaceumlauts }}"

NAME_FORMAT_NAME = "name"
NAME_FORMAT_NAME_TEMPLATE = "{{ name | default('', True) }}"

NAME_FORMAT_NAME_5_CHAR = "name-5-char"
NAME_FORMAT_NAME_5_CHAR_TEMPLATE = "{{ name | default('', True) | replaceumlauts | removespaces | substring(0,4) | ljust(4) }}{{ mode_short }}"

NAME_FORMAT_NAME_6_CHAR = "name-6-char"
NAME_FORMAT_NAME_6_CHAR_TEMPLATE = "{{ name | default('', True) | replaceumlauts | removespaces | substring(0,4) | ljust(4) }}-{{ mode_short }}"

NAME_FORMAT_NAME_7_CHAR = "name-7-char"
NAME_FORMAT_NAME_7_CHAR_TEMPLATE = "{{ name | default('', True) | replaceumlauts | removespaces | substring(0,5) | ljust(4) }}-{{ mode_short }}"

NAME_FORMAT_NAME_CALLSIGN = "name-callsign"
NAME_FORMAT_NAME_CALLSIGN_TEMPLATE = "{{ name | default('', True) | replaceumlauts | removespaces | substring(0,7) | ljust(7) }}{{ '-' if callsign is defined and callsign | length }}{{ callsign | default('', True) | removeprefix('OE') | substring(0,4) }}"

NAME_FORMAT_NAME_MODE_CALLSIGN = "name-mode-callsign"
NAME_FORMAT_NAME_MODE_CALLSIGN_TEMPLATE = "{{ name | default('', True) | replaceumlauts | removespaces | substring(0,5) | ljust(5) }}-{{ mode_short }}{{ '-' if callsign is defined and callsign | length }}{{ callsign | default('', True) | removeprefix('OE') | substring(0,4) }}"

NAME_FORMAT_CUSTOM = "custom"


class ArgParseFilterCustom(argparse.Action):
    def __call__(self, parser, namespace, values, option_string=None):
        # Force attribute to type dict
        if not isinstance(getattr(namespace, self.dest), dict):
            setattr(namespace, self.dest, dict())
        for value in values:
            key, value = value.split('=')
            getattr(namespace, self.dest)[key.strip()] = value.strip()


class MemoryChannelProcessor(object):
    def __init__(self):

        """ Main execution path """

        self.root_path = path_resolve(os.path.join(os.getcwd(), os.path.dirname(__file__)))
        self.filename = os.path.basename(__file__)

        # Parse CLI arguments
        parser = self.args_parser_prepare()
        self.args = parser.parse_args()


        # Set defaults if needed - Workaround for "argparse: append action with default list adds to list instead of overriding" (see https://bugs.python.org/issue16399)
        if is_empty(self.args.filter_types):
            self.args.filter_types = [FILTER_TYPE_FM]
        self.args.filter_types = flatten_array(self.args.filter_types)

        if is_empty(self.args.data_sorting):
            self.args.data_sorting = ['freq_tx', 'name', 'callsign']
        self.args.data_sorting = flatten_array(self.args.data_sorting)


        # Select the data sources
        self.data_sources_flat = strip_duplicates(flatten_array(self.args.data_sources))

        self.col_duplicate_check = (
            'freq_tx',
            'freq_rx',
            'callsign'
        )

        # Set logging
        if self.args.verbose:
            import logging
            logging.basicConfig()
            #logging.basicConfig(level='WARNING')
            logger = logging.getLogger()
            logger.setLevel('DEBUG')

            logger = logging.getLogger('requests_cache')
            logger.setLevel('DEBUG')
            logger.propagate = True

            from http.client import HTTPConnection
            HTTPConnection.debuglevel = 1
            logger = logging.getLogger("urllib3")
            logger.setLevel(logging.DEBUG)
            logger.propagate = True

    def process(self):
        # Print out version and exit
        if self.args.version:
            from memory import __version__
            print(__version__)
            exit(0)

        # Print out AsciiDoc and exit
        if not is_empty(self.args.asciidoc):
            self.print_asciidoc(self.args.asciidoc)
            exit(0)

        # Check for data sources
        if is_empty(self.data_sources_flat):
            print("Please specify at least one data source!")
            exit(1)

        # ================
        # Preparation
        # ================
        # Setup caching of web requests
        if self.args.request_cache_enabled:
            self.print_verbose("Configure requests cache")
            requests_cache.install_cache("memory_channels_processor_cache", expire_after=10 * 60, backend='sqlite', serializer='json')

        # ================
        # Collect data from data sources
        # ================
        self.print_verbose(f"Initializing data sources: %s" % self.data_sources_flat)

        data_frames = []
        for data_source in self.data_sources_flat:
            self.print_verbose("============")
            self.print_verbose(f"Data source '%s': Processing" % data_source)

            data_sources = get_data_sources()
            if data_source in data_sources.keys():
                data_source_instance = data_sources[data_source]
                data_source_dataframe = data_source_instance.get_data(self, self.args)
            else:
                data_source_dataframe = None

            if data_source_dataframe is None or data_source_dataframe.empty:
                self.print_verbose(f"Data source '%s': Returned an empty dataset" % data_source)
            else:
                self.print_verbose(f"Data source '%s': Appending dataset" % data_source)
                data_frames.append(data_source_dataframe)

        # Remove caching of web requests
        if self.args.request_cache_enabled:
            self.print_verbose("Remove requests cache")
            requests_cache.uninstall_cache()

        # ================
        # Append dataframes
        # ================
        if not is_empty(data_frames):
            with warnings.catch_warnings():
                # TODO: (Pandas 2.1.0+) Fix deprecated functionality "FutureWarning: The behavior of DataFrame concatenation with empty or all-NA entries is deprecated."
                warnings.filterwarnings("ignore", category=FutureWarning)
                data = pd.concat(data_frames, ignore_index=True)
        else:
            data = None

        # ================
        # Check for empty
        # ================
        if data is None or data.empty:
            print("The data source(s) returned an empty dataset!")
            exit(1)

        # ================
        # Calculate data
        # ================
        # Calculate - Offset + DUP + Callsign based infos
        data['offset'] = data.apply(self.calc_offset, axis=1)
        data['dup'] = data.apply(self.calc_dup, axis=1)
        data['name_formatted'] = data.apply(self.calc_name_formatted, axis=1)
        #data.apply(self.calc_callsign_infos, axis=1)

        # Convert data types
        data['ctcss_tx'] = data['ctcss_tx'].replace(np.nan, None)
        data['ctcss_rx'] = data['ctcss_rx'].replace(np.nan, None)

        # Calculate - Locator and Distance
        data['locator'] = data.apply(self.calc_locator, axis=1)
        if not is_empty(self.args.locator):
            self.print_verbose(f"Calculate distance to locator: %s" % self.args.locator)
            data['distance'] = data.apply(lambda row: self.calc_distance(row, self.args.locator), axis=1)
            data['heading'] = data.apply(lambda row: self.calc_heading(row, self.args.locator), axis=1)
        else:
            data['distance'] = None
            data['heading'] = None

        # ================
        # Filter
        # ================
        filter_bands_flat = strip_duplicates(flatten_array(self.args.filter_bands))
        filter_types_flat = strip_duplicates(flatten_array(self.args.filter_types))

        # Filtering - filter invalid values
        data_query = ['not band.isnull()']

        # Filtering - include only values defined in args
        if not is_empty(filter_bands_flat):
            self.print_verbose(f"Filter for bands: %s" % filter_bands_flat)
            data_query.append(f"band in %s" % filter_bands_flat)

        # Filtering - item in distance
        if not is_empty(self.args.filter_distance_max):
            self.print_verbose(f"Filter for distance max: %s" % self.args.filter_distance_max)
            data_query.append(f"(distance.isnull() or distance <= %s)" % self.args.filter_distance_max)

        # Filtering - FM, D-STAR, DMR
        data_query_or = []
        if FILTER_TYPE_FM in filter_types_flat:
            self.print_verbose("Filter for FM repeaters")
            data_query_or.append("fm")

        if FILTER_TYPE_DSTAR in filter_types_flat:
            self.print_verbose("Filter for D-STAR repeaters")
            data_query_or.append("dstar")

        if FILTER_TYPE_DMR in filter_types_flat:
            self.print_verbose("Filter for DMR repeaters")
            data_query_or.append("dmr")

        if not is_empty(data_query_or):
            data_query_or_str = '(' + ') or ('.join(data_query_or) + ')'
            data_query.append(data_query_or_str)

        # Filtering - apply custom filters
        if not is_empty(self.args.filter_custom):
            self.print_verbose(f"Apply custom filters: %s" % self.args.filter_custom)
            for key in self.args.filter_custom:
                if key in get_data_filterable_col_names():
                    #print(f"KEY: '%s' - VALUE: '%s'" % (key, self.args.filter_custom[key]))
                    filter_query = f"%s.str.contains(\"%s\")" % (key, self.args.filter_custom[key])
                    #print(f"QUERY: '%s'" % filter_query)
                    data_query.append(filter_query)
                else:
                    self.print_verbose(f"Filtering for key '%s' not possible. Key not in: %s" % (key, ", ".join(get_data_filterable_col_names())))

        if not is_empty(data_query):
            data_query_str = '(' + ') and ('.join(data_query) + ')'
            self.print_verbose(f"Filter dataset: %s" % data_query_str)
            data.query(data_query_str, inplace=True)

        # ================
        # Sort
        # ================
        if not is_empty(self.args.data_sorting):
            self.print_verbose(f"Sort dataset %s using %s" % ("ascending" if self.args.data_sorting_ascending else "descending", self.args.data_sorting))
            data.sort_values(by=self.args.data_sorting, ascending=self.args.data_sorting_ascending, inplace=True)

        # Duplicates
        self.print_verbose("Clean dataset from duplicates")
        data.drop_duplicates(subset=self.col_duplicate_check, inplace=True)

        # ================
        # Reset index
        # ================
        data.reset_index(drop=True, inplace=True)

        # ================
        # Output
        # ================
        output_format = self.args.output_format

        if self.args.verbose:
            print("============")
            print("Dataset:")
            data.info()
            print(data)

        data_targets = get_data_targets()
        if output_format in data_targets.keys():
            self.print_verbose("============")
            self.print_verbose(f"Data target '%s': Processing" % output_format)
            data_target = data_targets[output_format]
            data_target.store_data(self, self.args, data)
            self.print_verbose("============")
        else:
            print("No output format was specified")
            exit(1)

        #print("Done")
        exit(0)

    def calc_offset(self, row):
        return round(row['freq_tx'] - row['freq_rx'], 2)

    def calc_dup(self, row):
        if row['offset'] is None:
            result = None
        elif row['offset'] > 0:
            result = '-'
        elif row['offset'] < 0:
            result = '+'
        else:
            result = None

        return result

    def calc_name_formatted(self, row):
        result = None

        # - Label by callsign ------
        # --------------------------

        callsign = row['callsign']
        name = replace_umlauts(row['name'])

        if self.args.name_format == NAME_FORMAT_CALLSIGN:
            if not is_empty(callsign):
                result = callsign

        elif self.args.name_format == NAME_FORMAT_CALLSIGN_5_CHAR:
            result = self.calc_name_formatted_custom(NAME_FORMAT_CALLSIGN_5_CHAR_TEMPLATE, row)

        elif self.args.name_format == NAME_FORMAT_CALLSIGN_6_CHAR:
            result = self.calc_name_formatted_custom(NAME_FORMAT_CALLSIGN_6_CHAR_TEMPLATE, row)

        elif self.args.name_format == NAME_FORMAT_CALLSIGN_7_CHAR:
            result = self.calc_name_formatted_custom(NAME_FORMAT_CALLSIGN_7_CHAR_TEMPLATE, row)

        elif self.args.name_format == NAME_FORMAT_CALLSIGN_NAME:
            result = self.calc_name_formatted_custom(NAME_FORMAT_CALLSIGN_NAME_TEMPLATE, row)

        elif self.args.name_format == NAME_FORMAT_CALLSIGN_MODE_NAME:
            result = self.calc_name_formatted_custom(NAME_FORMAT_CALLSIGN_MODE_NAME_TEMPLATE, row)

        # - Label by name ----------
        # --------------------------

        elif self.args.name_format == NAME_FORMAT_NAME:
            if not is_empty(name):
                result = name

        elif self.args.name_format == NAME_FORMAT_NAME_5_CHAR:
            result = self.calc_name_formatted_custom(NAME_FORMAT_NAME_5_CHAR_TEMPLATE, row)

        elif self.args.name_format == NAME_FORMAT_NAME_6_CHAR:
            result = self.calc_name_formatted_custom(NAME_FORMAT_NAME_6_CHAR_TEMPLATE, row)

        elif self.args.name_format == NAME_FORMAT_NAME_7_CHAR:
            result = self.calc_name_formatted_custom(NAME_FORMAT_NAME_7_CHAR_TEMPLATE, row)

        elif self.args.name_format == NAME_FORMAT_NAME_CALLSIGN:
            result = self.calc_name_formatted_custom(NAME_FORMAT_NAME_CALLSIGN_TEMPLATE, row)

        elif self.args.name_format == NAME_FORMAT_NAME_MODE_CALLSIGN:
            result = self.calc_name_formatted_custom(NAME_FORMAT_NAME_MODE_CALLSIGN_TEMPLATE, row)

        # - Label by custom scheme -
        # --------------------------

        elif self.args.name_format == NAME_FORMAT_CUSTOM:
            if not is_empty(self.args.name_format_custom):
                result = self.calc_name_formatted_custom(self.args.name_format_custom, row)

        # --------------------------

        return result

    def calc_name_formatted_custom(self, template: str, row):
        from jinja2 import (
            Environment,
            BaseLoader,
            StrictUndefined,
        )

        env = Environment(
            loader=BaseLoader,
            extensions=['jinja2.ext.debug'],
            keep_trailing_newline=True,
            trim_blocks=False,
            lstrip_blocks=False,
            optimized=False,
        )

        env.undefined = StrictUndefined

        # Register custom functions/functions
        custom_jinja_functions = dict()
        custom_jinja_functions['replaceumlauts'] = filter_replace_umlauts
        custom_jinja_functions['replace_umlauts'] = filter_replace_umlauts
        custom_jinja_functions['removediacritic'] = filter_remove_diacritic
        custom_jinja_functions['remove_diacritic'] = filter_remove_diacritic
        custom_jinja_functions['removespaces'] = filter_remove_spaces
        custom_jinja_functions['remove_spaces'] = filter_remove_spaces
        custom_jinja_functions['ljust'] = lambda x, y, z = ' ': str(x).ljust(y, z)
        custom_jinja_functions['lstrip'] = lambda x, y = ' ': str(x).lstrip(y)
        custom_jinja_functions['removeprefix'] = filter_remove_prefix
        custom_jinja_functions['remove_prefix'] = filter_remove_prefix
        custom_jinja_functions['removesuffix'] = filter_remove_suffix
        custom_jinja_functions['remove_suffix'] = filter_remove_suffix
        custom_jinja_functions['rjust'] = lambda x, y, z = ' ': str(x).rjust(y, z)
        custom_jinja_functions['rstrip'] = lambda x, y = ' ': str(x).rstrip(y)
        custom_jinja_functions['strip'] = lambda x, y = ' ': str(x).strip(y)
        custom_jinja_functions['swapcase'] = lambda x: str(x).swapcase()
        custom_jinja_functions['substring'] = lambda x, y, z = None: str(x)[y:z]

        # Add as functions
        env.globals.update(custom_jinja_functions)

        # Add as filters
        env.filters.update(custom_jinja_functions)

        # Prepare variables
        custom_jinja_vars = dict()
        custom_jinja_vars.update(row.fillna('').to_dict())
        custom_jinja_vars['mode_short'] = row['band'][:1] if row['fm'] else 'D'

        # Process the string
        result = env.from_string(template).render(custom_jinja_vars)

        # Strip all newlines
        result = result.replace('\n', ' ').replace('\r', '')

        return result

    def calc_locator(self, row):
        if row['lat'] is not None and row['long'] is not None:
            try:
                result = latlong_to_locator(row['lat'], row['long'])
            except:
                result = None
        else:
            result = None

        return result

    def calc_distance(self, row, locator):
        if row['locator'] is not None:
            try:
                result = round(calculate_distance(locator, row['locator']), 2)
            except:
                result = None
        else:
            result = None

        return result

    def calc_heading(self, row, locator):
        if row['locator'] is not None:
            try:
                result = round(calculate_heading(locator, row['locator']), 2)
            except:
                result = None
        else:
            result = None

        return result

    def calc_callsign_infos(self, row):
        #TODO: Get additional infos via pyhamlib
        result = {
            'test1': 0,
            'test2': 1
        }

        return pd.Series(result)

    def args_parser_prepare(self) -> argparse.ArgumentParser:
        parser = argparse.ArgumentParser(description='Process memory channels', allow_abbrev=True, formatter_class=argparse.ArgumentDefaultsHelpFormatter)

        # Input / Output
        parser.add_argument('--source', dest='data_sources', action='append', choices=sorted(get_data_sources().keys()), help='The data source', nargs='+', type=str.lower, default=[])
        parser.add_argument('--output-file', dest='output_file', action='store', help='The output file', metavar="<file>", type=argparse.FileType('wb'))
        parser.add_argument('--output-format', dest='output_format', action='store', choices=sorted(get_data_targets().keys()), help='The output format', type=str.lower, default='csv')

        # Filtering
        parser.add_argument('--band', dest='filter_bands', action='append', choices=get_band_frequency_ranges().keys(), help='Filter for a specific band', nargs='*', type=str.lower)
        parser.add_argument('--type', dest='filter_types', action='append', choices=[FILTER_TYPE_FM, FILTER_TYPE_DSTAR, FILTER_TYPE_DMR], help='Filter for a specific type', nargs='*', type=str.lower, default=[])
        parser.add_argument('--distance-max', dest='filter_distance_max', action='store', help='Filter for a specific maximum distance', metavar="<distance>", type=check_positive, default=None)
        parser.add_argument('--filter', dest='filter_custom', action=ArgParseFilterCustom, help='Filter for specific key value combinations (Available keys: ' + ", ".join(get_data_filterable_col_names()) + ')', metavar="<key>=<value>", nargs='*', type=str, default=dict())

        # Sorting
        parser.add_argument('--sort', dest='data_sorting', action='append', choices=sorted(get_data_sortable_col_names()), help='The key used for sorting', nargs='+', type=str.lower, default=[])
        parser.add_argument('--sort-descending', dest='data_sorting_ascending', action='store_false', help='The sorting direction', default=True)

        # Additional arguments
        parser.add_argument('--locator', dest='locator', action='store', help='The locator used as reference for calculating distances', metavar="<locator>", type=str, default=None)

        # Formatting
        parser.add_argument('--name-format', '--chirp-name', dest='name_format', action='store', choices=[NAME_FORMAT_CALLSIGN, NAME_FORMAT_CALLSIGN_5_CHAR, NAME_FORMAT_CALLSIGN_6_CHAR, NAME_FORMAT_CALLSIGN_7_CHAR, NAME_FORMAT_NAME, NAME_FORMAT_NAME_5_CHAR, NAME_FORMAT_NAME_6_CHAR, NAME_FORMAT_NAME_7_CHAR, NAME_FORMAT_CALLSIGN_NAME, NAME_FORMAT_CALLSIGN_MODE_NAME, NAME_FORMAT_NAME_CALLSIGN, NAME_FORMAT_NAME_MODE_CALLSIGN, NAME_FORMAT_CUSTOM], help='The name format', type=str.lower, default=NAME_FORMAT_CALLSIGN)
        parser.add_argument('--name-format-custom', dest='name_format_custom', action='store', help='The custom name format', metavar="<name format template>", type=str, default='')

        # Add additional arguments from data sources and targets
        data_sources = get_data_sources()
        for item in data_sources.keys():
            data_source = data_sources[item]
            data_source.setup_args(parser)

        data_targets = get_data_targets()
        for item in data_targets.keys():
            data_target = data_targets[item]
            data_target.setup_args(parser)

        # Flags
        parser.add_argument('--offline', dest='offline', action='store_true', help='Enable offline mode', default=False)
        parser.add_argument('--no-cache', dest='request_cache_enabled', action='store_false', help='Disable caching of web requests', default=True)
        parser.add_argument('--verbose', dest='verbose', action='store_true', help='Enable verbosity', default=False)
        parser.add_argument('--version', dest='version', action='store_true', help='Show the version', default=False)

        # Documentation (hidden)
        parser.add_argument('--asciidoc', dest='asciidoc', action='store', choices=[ASCIIDOC_DATA_SOURCE_COLUMNS, ASCIIDOC_DATA_TARGET_COLUMNS, ASCIIDOC_NAME_FORMATS], help=argparse.SUPPRESS, type=str.lower, default=None)

        return parser

    def print_verbose(self, output: AnyStr):
        """ Print out only if verbose is set """

        if self.args.verbose:
            print(output)

    def print_asciidoc(self, asciidoc_type: str):
        """ Print out AsciiDoc formatted content for documentation """

        if asciidoc_type == ASCIIDOC_DATA_SOURCE_COLUMNS:
            print("[%header,%autowidth,stripes=even,format=dsv,separator=;]")
            print("|===")
            print("Key;Type")
            columns = get_data_source_cols()
            for key in sorted(columns.keys()):
                print("# tag::" + key + "[]")
                print(key + ";" + columns[key])
                print("# end::" + key + "[]")
            print("|===")

        elif asciidoc_type == ASCIIDOC_DATA_TARGET_COLUMNS:
            print("[%header,%autowidth,stripes=even,format=dsv,separator=;]")
            print("|===")
            print("Key;Type")
            columns = get_data_sortable_cols()
            for key in sorted(columns.keys()):
                print("# tag::" + key + "[]")
                print(key + ";" + columns[key])
                print("# end::" + key + "[]")
            print("|===")

        elif asciidoc_type == ASCIIDOC_NAME_FORMATS:
            var_names = [
                "NAME_FORMAT_CALLSIGN",
                "NAME_FORMAT_CALLSIGN_5_CHAR",
                "NAME_FORMAT_CALLSIGN_6_CHAR",
                "NAME_FORMAT_CALLSIGN_7_CHAR",
                "NAME_FORMAT_CALLSIGN_NAME",
                "NAME_FORMAT_CALLSIGN_MODE_NAME",
                "NAME_FORMAT_NAME",
                "NAME_FORMAT_NAME_5_CHAR",
                "NAME_FORMAT_NAME_6_CHAR",
                "NAME_FORMAT_NAME_7_CHAR",
                "NAME_FORMAT_NAME_CALLSIGN",
                "NAME_FORMAT_NAME_MODE_CALLSIGN"
                ]

            print("[%header,%autowidth,stripes=even,format=dsv,separator=;,cols=\"1,3\"]")
            print("|===")
            print("Format;Template")
            for var_name in sorted(var_names):
                var_value = globals()[var_name]
                var_template_value = globals()[var_name + "_TEMPLATE"].replace(";", "\\;")

                print("# tag::" + var_value + "[]")
                print(var_value + ";" + var_template_value + "")
                print("# end::" + var_value + "[]")
            print("|===")


class AbstractSource(ABC):

    @abstractmethod
    def setup_args(self, parser: argparse.ArgumentParser):
        pass

    @abstractmethod
    def get_data(self, processor: MemoryChannelProcessor, args: argparse.Namespace) -> DataFrame:
        pass


class AbstractTarget(ABC):

    @abstractmethod
    def setup_args(self, parser: argparse.ArgumentParser):
        pass

    @abstractmethod
    def store_data(self, processor: MemoryChannelProcessor, args: argparse.Namespace, data: DataFrame):
        pass


def path_resolve(path: AnyStr) -> AnyStr:
    """ Resolves the given path """

    return os.path.realpath(os.path.normpath(os.path.abspath(path)))


def path_relativize(path: str, start: str) -> str:
    """ Relativize the given path """

    return os.path.relpath(path, start)


def is_empty(item: Any) -> bool:
    """ Check if the item is empty """

    if item is None:
        return True

    if isinstance(item, list) and len(item) == 0:
        return True
    elif item == '':
        return True

    return False


def push_array_unique(my_dict: dict, key: str, element: Any) -> None:
    """ Push an element onto an array that may not have been defined in the dict only if the element isn't
    already in the array"""

    if key in my_dict:
        if element not in my_dict[key]:
            my_dict[key].append(element)
    else:
        my_dict[key] = [element]


def push_set(my_dict: dict, key: str, element: Any) -> None:
    """ Push an element onto an array that may not have been defined in the dict only if the element isn't
    already in the array"""

    if key in my_dict:
        my_dict[key].add(element)
    else:
        my_dict[key] = {element}


def is_collection(some_collection: Any) -> bool:
    return isinstance(some_collection, Iterable)


def strip_duplicates(items: Iterable) -> []:
    return list(set(items))


def get_data_sources() -> dict:
    from memory.channels.data_source_fm_channel_iaru import FMChannelIARUR1GeneratorSource
    from memory.channels.data_source_oevsv_repeater_db import OevsvRepeaterDbSource
    from memory.channels.data_source_ha2to_repeater_list import HA2TORepeaterListSource
    data_sources = {
        # Generic
        "csv": memory.channels.CsvSource(),
        "tsv": memory.channels.TsvSource(),

        # Static channels
        "fm-channels-iaru-r1": FMChannelIARUR1GeneratorSource(),

        # ÖVSV
        "oevsv-repeater-db": OevsvRepeaterDbSource(),

        # HA2TO
        "ha2to-repeater-list": HA2TORepeaterListSource()
    }
    return data_sources


def get_data_targets() -> dict:
    from memory.channels.data_target_icom import IcomIC705ID52Target, IcomIC9700Target
    from memory.channels.data_target_chirp import ChirpTarget
    from memory.channels.data_target_opengd77 import OpenGD77Target

    icom_ic_705_target = IcomIC705ID52Target()
    data_targets = {
        # Generic
        "csv": memory.channels.CsvTarget(),
        "tsv": memory.channels.TsvTarget(),
        "xlsx": memory.channels.ExcelTarget(),

        # Icom
        "icom-ic-705": icom_ic_705_target,
        "icom-id-52": icom_ic_705_target,
        "icom-rs-ba1v2": icom_ic_705_target,
        "icom-rs-ms1": icom_ic_705_target,
        "icom-ic-9700": IcomIC9700Target(),

        # Chirp
        "chirp": ChirpTarget(),

        # OpenGD77
        "opengd77": OpenGD77Target()
    }
    return data_targets


def get_data_source_cols() -> dict:
    cols = {
        'callsign': 'object',
        'name': 'object',
        'band': 'object',
        'freq_tx': 'float64',
        'freq_rx': 'float64',
        'ctcss': 'bool',
        'ctcss_tx': 'float64',
        'ctcss_rx': 'float64',
        'dmr': 'bool',
        'dmr_id': 'int64',
        'dstar': 'bool',
        'dstar_rpt1': 'object',
        'dstar_rpt2': 'object',
        'fm': 'bool',
        'landmark': 'object',
        'state': 'object',
        'country': 'object',
        'loc_exact': 'bool',
        'lat': 'float64',
        'long': 'float64'
    }
    return cols


def get_data_source_col_names() -> []:
    return get_data_source_cols().keys()


def get_data_additional_cols() -> dict:
    cols = {
        'offset': 'float64',
        'dup': 'object',
        'name_formatted': 'object',
        'locator': 'object',
        'distance': 'float64',
        'heading': 'int64'
    }
    return cols


def get_data_additional_col_names() -> []:
    return get_data_additional_cols().keys()


def get_data_filterable_cols() -> dict:
    cols = {**get_data_source_cols(), **get_data_additional_cols()}
    cols = {key: val for key, val in cols.items() if val == 'object'}
    return cols


def get_data_filterable_col_names() -> []:
    return get_data_filterable_cols().keys()


def get_data_sortable_cols() -> dict:
    return {**get_data_source_cols(), **get_data_additional_cols()}


def get_data_sortable_col_names() -> []:
    return get_data_source_col_names() | get_data_additional_col_names()


def get_band_frequency_ranges() -> dict:
    frequency_ranges = {
        "13cm": [2320, 2450],
        "23cm": [1240, 1300],
        "70cm": [430, 440],
        "2m": [144, 146],
        "6m": [50.4, 52],
        "10m": [28, 29.7],
        "12m": [24.89, 24.99],
        "15m": [21, 21.45],
        "20m": [14, 14.35],
        "40m": [7, 7.2],
        "80m": [3.5, 3.8]
    }
    return frequency_ranges


def get_band_by_freq(frequency: float) -> Any:
    keys = [k for k, v in get_band_frequency_ranges().items() if v[0] <= frequency <= v[1]]
    if not is_empty(keys):
        return keys[0]

    return None


def flatten_array(items: Iterable) -> []:
    result = []
    if isinstance(items, str) and not is_empty(items):
        result.append(items)
    elif not is_empty(items):
        for i in items:
            if isinstance(i, str) and not is_empty(i):
                result.append(i)
            elif is_collection(i):
                for j in flatten_array(i):
                    if not is_empty(j):
                        result.append(j)
            else:
                print(type(i))

    return result


def to_safe(word: Any) -> str:
    """ Converts 'bad' characters in a string to underscores """

    result = re.sub(r"[^A-Za-z0-9]", "_", str(word))

    # Remove duplicate underscores
    result = result.replace("__", "_").replace("__", "_")

    # Remove leading and trailing underscores
    result = result.strip("_")

    return result


def force_text(data):
    if isinstance(data, str):
        return data
    if isinstance(data, bytes):
        return data.decode('utf8')
    return data


def remove_spaces(x: str) -> str:
    """Replace spaces from text."""
    if is_empty(x):
        return ''

    return x.replace(' ', '')


def filter_remove_spaces(x: str) -> str:
    return remove_spaces(x)


def remove_diacritic(x: str) -> str:
    from unidecode import unidecode

    if is_empty(x):
        return ""

    return unidecode(x)


def filter_remove_diacritic(x: str) -> str:
    return remove_diacritic(x)


def replace_umlauts(x: str) -> str:
    """Replace special German umlauts (vowel mutations) from text.
        ä -> ae, Ä -> Ae...
        ü -> ue, Ü -> Ue...
        ö -> oe, Ö -> Oe...
        ß -> ss
        """
    if is_empty(x):
        return ''

    vowel_char_map = {
        ord('ä'): 'ae', ord('ü'): 'ue', ord('ö'): 'oe', ord('ß'): 'ss',
        ord('Ä'): 'Ae', ord('Ü'): 'Ue', ord('Ö'): 'Oe'
    }
    return x.translate(vowel_char_map)


def filter_replace_umlauts(x: str) -> str:
    return replace_umlauts(x)


def filter_remove_prefix(x: str, y: str) -> str:
    if PY3_9_OR_LATER:
        return x.removeprefix(y) if x else x
    else:
        if x and x.startswith(y):
            return x[len(y):]
        else:
            return x


def filter_remove_suffix(x: str, y: str) -> str:
    if PY3_9_OR_LATER:
        return x.removesuffix(y) if x else x
    else:
        if x and x.endswith(y):
            return x[:-len(y)]
        else:
            return x


def check_positive(value):
    int_value = int(value)
    if int_value <= 0:
        raise argparse.ArgumentTypeError("%s is an invalid positive int value" % value)
    return int_value


def check_negative(value):
    int_value = int(value)
    if int_value >= 0:
        raise argparse.ArgumentTypeError("%s is an invalid negative int value" % value)
    return int_value


def main():
    processor = MemoryChannelProcessor()
    processor.process()


if __name__ == "__main__":
    main()
