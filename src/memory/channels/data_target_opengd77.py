import argparse

import numpy as np
from pandas import DataFrame

from memory.channels.data_targets import Jinja2BaseTarget
from memory.channels.processor import is_empty


class OpenGD77Target(Jinja2BaseTarget):

    def setup_args(self, parser: argparse.ArgumentParser):
        Jinja2BaseTarget.setup_args(self, parser)

    def prepare_data(self, data: DataFrame, args: argparse.Namespace) -> DataFrame:
        return data

    def get_template_path(self, args: argparse.Namespace) -> str:
        return "opengd77_channels.csv.tpl"
