import argparse

import numpy as np
from pandas import DataFrame

from memory.channels.data_targets import Jinja2BaseTarget
from memory.channels.processor import is_empty

ICOM_TYPE_FM = "fm"
ICOM_TYPE_DSTAR = "d-star"


class IcomBaseTarget(Jinja2BaseTarget):

    def setup_args(self, parser: argparse.ArgumentParser):
        Jinja2BaseTarget.setup_args(self, parser)
        if self.arg_undefined(parser, '--icom-type'):
            parser.add_argument('--icom-type', dest='icom_type', action='store', choices=[ICOM_TYPE_FM, ICOM_TYPE_DSTAR], help='The output type', type=str.lower, default=ICOM_TYPE_FM)
        if self.arg_undefined(parser, '--icom-group-number'):
            parser.add_argument('--icom-group-number', dest='icom_group_number', action='store', help='The group number', metavar='<number>', type=int, default=0)
        if self.arg_undefined(parser, '--icom-group-name'):
            parser.add_argument('--icom-group-name', dest='icom_group_name', action='store', help='The group name', metavar='<name>', type=str, default="Default Group Name")

    def prepare_data(self, data: DataFrame, args: argparse.Namespace) -> DataFrame:
        # Set values
        data.loc[data['offset'] > 0, 'dup'] = "DUP-"
        data.loc[data['offset'] < 0, 'dup'] = "DUP+"

        # Calculate - Tone
        data['tone'] = data.apply(self.get_tone, args=(args,), axis=1)

        return data

    def get_tone(self, row, args: argparse.Namespace) -> str:
        result = ""
        if args.icom_type == ICOM_TYPE_FM:
            if (not is_empty(row['ctcss_rx']) and row['ctcss_rx'] > 0) and (not is_empty(row['ctcss_tx']) and row['ctcss_tx'] > 0):
                result = "TONE(T)/TSQL(R)"
            elif not is_empty(row['ctcss_rx']) and row['ctcss_rx'] > 0:
                result = "TONE"
            else:
                result = "OFF"
        elif args.icom_type == ICOM_TYPE_DSTAR:
            if not is_empty(row['ctcss_rx']) and row['ctcss_rx'] > 0:
                result = "Tone"
            else:
                result = "None"

        return result


class IcomIC705ID52Target(IcomBaseTarget):

    def get_template_path(self, args: argparse.Namespace) -> str:
        result = None
        if args.icom_type == ICOM_TYPE_FM:
            result = "icom_ic705_id52_fm.csv.tpl"
        elif args.icom_type == ICOM_TYPE_DSTAR:
            result = "icom_ic705_id52_dstar.csv.tpl"

        return result


class IcomIC9700Target(IcomBaseTarget):

    def get_template_path(self, args: argparse.Namespace) -> str:
        result = None
        if args.icom_type == ICOM_TYPE_FM:
            result = "icom_ic9700_fm.csv.tpl"
        elif args.icom_type == ICOM_TYPE_DSTAR:
            result = "icom_ic9700_dstar.csv.tpl"

        return result
