import argparse
import os
import math
from _csv import QUOTE_MINIMAL

from pandas import DataFrame

from memory.channels.processor import is_empty, force_text, filter_replace_umlauts, filter_remove_diacritic, \
    filter_remove_spaces, AbstractTarget, filter_remove_prefix, filter_remove_suffix
from memory.channels import MemoryChannelProcessor


class CsvTarget(AbstractTarget):

    def setup_args(self, parser: argparse.ArgumentParser):
        pass

    def store_data(self, processor: MemoryChannelProcessor, args: argparse.Namespace, data: DataFrame):
        if is_empty(args.output_file):
            result = data.to_csv(path_or_buf=None, index=False, header=True, quoting=QUOTE_MINIMAL, encoding='utf-8')
            print(result)
        else:
            output_file = os.path.realpath(args.output_file.name)
            processor.print_verbose(f"Writing out to: %s" % output_file)
            with open(file=output_file, mode='wb') as f:
                data.to_csv(path_or_buf=f, index=False, header=True, quoting=QUOTE_MINIMAL, encoding='utf-8', lineterminator='\r\n')


class TsvTarget(AbstractTarget):

    def setup_args(self, parser: argparse.ArgumentParser):
        pass

    def store_data(self, processor: MemoryChannelProcessor, args: argparse.Namespace, data: DataFrame):
        if is_empty(args.output_file):
            result = data.to_csv(path_or_buf=None, index=False, header=True, quoting=QUOTE_MINIMAL, encoding='utf-8', sep='\t')
            print(result)
        else:
            output_file = os.path.realpath(args.output_file.name)
            processor.print_verbose(f"Writing out to: %s" % output_file)
            with open(file=output_file, mode='wb') as f:
                data.to_csv(path_or_buf=f, index=False, header=True, quoting=QUOTE_MINIMAL, encoding='utf-8', sep='\t', lineterminator='\r\n')


class ExcelTarget(AbstractTarget):

    def setup_args(self, parser: argparse.ArgumentParser):
        pass

    def store_data(self, processor: MemoryChannelProcessor, args: argparse.Namespace, data: DataFrame):
        if is_empty(args.output_file):
            print("No output file given")
            exit(1)

        output_file = os.path.realpath(args.output_file.name)
        processor.print_verbose(f"Writing out to: %s" % output_file)
        data.to_excel(output_file, engine='xlsxwriter', sheet_name='memory-channels', index=False, header=True)


class Jinja2BaseTarget(AbstractTarget):

    def setup_args(self, parser: argparse.ArgumentParser):
        if self.arg_undefined(parser, '--jinja2-extension'):
            parser.add_argument('--jinja2-extension', dest='jinja2_extensions', action='append', help='Jinja2 extension name', metavar='<name>', nargs='+', type=str, default=[])
        if self.arg_undefined(parser, '--jinja2-lenient'):
            parser.add_argument('--jinja2-lenient', dest='jinja2_lenient', action='store_true', help='Disable strict mode', default=False)

    def arg_is_defined(self, parser: argparse.ArgumentParser, option: str):
        return any(option in x.option_strings for x in parser.__dict__['_actions'])

    def arg_undefined(self, parser: argparse.ArgumentParser, option: str):
        return not self.arg_is_defined(parser, option)

    def store_data(self, processor: MemoryChannelProcessor, args: argparse.Namespace, data: DataFrame):
        from jinja2 import (
            __version__ as jinja_version,
            Environment,
            FileSystemLoader,
            StrictUndefined,
        )

        extensions = []
        for ext in args.jinja2_extensions:
            # Allow shorthand and assume if it's not a module
            # path, it's probably trying to use builtin from jinja2
            if "." not in ext:
                ext = "jinja2.ext." + ext
            extensions.append(ext)

        # Starting with jinja2 3.1, `with_` and `autoescape` are no longer
        # able to be imported, but since they were default, let's stub them back
        # in implicitly for older versions.
        # We also don't track any lower bounds on jinja2 as a dependency, so
        # it's not easily safe to know it's included by default either.
        if tuple(jinja_version.split(".", 2)) < ("3", "1"):
            for ext in "with_", "autoescape":
                ext = "jinja2.ext." + ext
                if ext not in extensions:
                    extensions.append(ext)

        processor.print_verbose(f"Using template folder: %s" % processor.root_path)
        env = Environment(
            loader=FileSystemLoader(processor.root_path, encoding='utf-8'),
            extensions=extensions,
            keep_trailing_newline=True,
            #trim_blocks=True,
            #lstrip_blocks=True,
        )
        if args.jinja2_lenient:
            processor.print_verbose("Lenient mode")
        else:
            env.undefined = StrictUndefined
            processor.print_verbose("Strict mode")

        # Add environ global
        env.globals["environ"] = lambda key: force_text(os.environ.get(key))
        env.globals["get_context"] = lambda: data

        # Register custom functions/functions
        custom_jinja_functions = dict()
        custom_jinja_functions['format_ctcss'] = self.filter_format_ctcss
        custom_jinja_functions['format_coordinate'] = self.filter_format_coordinate
        custom_jinja_functions['format_callsign_dstar'] = self.filter_format_callsign_dstar
        custom_jinja_functions['replaceumlauts'] = filter_replace_umlauts
        custom_jinja_functions['replace_umlauts'] = filter_replace_umlauts
        custom_jinja_functions['removediacritic'] = filter_remove_diacritic
        custom_jinja_functions['remove_diacritic'] = filter_remove_diacritic
        custom_jinja_functions['removespaces'] = filter_remove_spaces
        custom_jinja_functions['remove_spaces'] = filter_remove_spaces
        custom_jinja_functions['removeprefix'] = filter_remove_prefix
        custom_jinja_functions['remove_prefix'] = filter_remove_prefix
        custom_jinja_functions['removesuffix'] = filter_remove_suffix
        custom_jinja_functions['remove_suffix'] = filter_remove_suffix

        # Add as functions
        env.globals.update(custom_jinja_functions)

        # Add as filters
        env.filters.update(custom_jinja_functions)

        template_path = self.get_template_path(args)
        if is_empty(template_path):
            print("Unable to get a template for the specified options!")
            exit(1)

        processor.print_verbose(f"Prepare data")
        prepared_data = self.prepare_data(data, args)
        if prepared_data is not None:
            data = prepared_data

        processor.print_verbose(f"Using template: %s" % template_path)
        result = env.get_template(os.path.basename(template_path)).render(args=args, data=data)

        if is_empty(args.output_file):
            print(result)
        else:
            output_file = os.path.realpath(args.output_file.name)
            processor.print_verbose(f"Writing out to: %s" % output_file)
            with open(file=output_file, mode='w', encoding='utf-8') as f:
                f.write(result)

    def prepare_data(self, data: DataFrame, args: argparse.Namespace) -> DataFrame:
        pass

    def get_template_path(self, args: argparse.Namespace) -> str:
        pass

    def filter_format_ctcss(self, x: any) -> str:
        if is_empty(x):
            return ""
        elif isinstance(x, str):
            return x.replace("Hz", "")
        elif isinstance(x, float) or isinstance(x, int):
            return str(x)

    def filter_format_coordinate(self, x: any, precision: int = None) -> str:
        if is_empty(x):
            return ""
        elif isinstance(x, str):
            return x.strip()
        elif isinstance(x, float):
            if math.isnan(x):
                return ""
            else:
                if precision is not None:
                    return str(round(x, precision))
                else:
                    return str(x)
        elif isinstance(x, int):
            return str(x)

    def filter_format_callsign_dstar(self, x: str, id: str) -> str:
        if is_empty(x):
            return ""
        elif is_empty(id):
            result = (x[:8]) if len(x) > 8 else x
            return result.upper()
        else:
            result = (x[:7]) if len(x) > 7 else x
            result = result.ljust(7)
            identifier = (id[:1]) if len(x) > 1 else id
            return result.upper() + identifier.upper()
