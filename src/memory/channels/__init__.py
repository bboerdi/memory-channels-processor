import sys
PY3_OR_LATER = sys.version_info[0] >= 3
PY3_8_OR_LATER = sys.version_info[0] >= 3 and sys.version_info[1] >= 8
PY3_9_OR_LATER = sys.version_info[0] >= 3 and sys.version_info[1] >= 9

from .processor import main  # NOQA
from .processor import MemoryChannelProcessor, AbstractSource, AbstractTarget
from .data_sources import CsvSource, TsvSource
from .data_targets import CsvTarget, TsvTarget, ExcelTarget
