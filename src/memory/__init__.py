"""
memory-channels-processor
==========

License: Apache, see LICENSE for more details.
"""

__all__ = ['channels', '__version__', '__git_url__', '__user_agent__']

__author__ = "OE3LRT"
__version__ = "0.0.13"
__git_url__ = "https://gitlab.com/oe3lrt/memory-channels-processor"
__user_agent__ = f"memory-channels-processor (v%s; %s)" % (__version__, __git_url__)
