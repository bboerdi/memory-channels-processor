# -*- coding: utf-8 -*-
from setuptools import setup, find_packages
from glob import glob
from os.path import basename
from os.path import dirname
from os.path import join
from os.path import realpath
from os.path import splitext

with open(join(dirname(__file__), 'VERSION'), 'r') as version_file:
    VERSION = version_file.read().strip()

with open(join(dirname(__file__), 'PACKAGE_NAME'), 'r') as package_file:
    PACKAGE_NAME = package_file.read().strip()

with open(join(dirname(__file__), 'requirements.in'), 'r') as requirements_file:
    REQUIRES_FILE = realpath(requirements_file.name)
    INSTALL_REQUIRES = list(filter(lambda x: not (x.startswith("#")), requirements_file.read().splitlines()))

README = join(dirname(__file__), 'README.rst')


def readme(file_path) -> str:
    with open(file_path, 'r') as f:
        return f.read()


setup(
    name=PACKAGE_NAME,
    version=VERSION,
    author='OE3LRT',
    author_email='lukas@oe3lrt.at',
    packages=find_packages(where='src'),
    package_dir={
        '': 'src',
    },
    py_modules=[splitext(basename(path))[0] for path in glob('src/*.py')],
    include_package_data=True,
    scripts=[],
    url='',
    license='LICENSE.txt',
    description='Memory Channels Processor',
    long_description=readme(README),
    long_description_content_type='text/x-rst',
    install_requires=INSTALL_REQUIRES,
    setup_requires=[
        'pytest-runner',
    ],
    python_requires='>=3',
    entry_points={
        'console_scripts': [
            'memory-channels-processor = memory.channels.processor:main'
        ]
    },
    # See https://pypi.org/classifiers/
    classifiers=[
        # How mature is this project? Common values are
        #   3 - Alpha
        #   4 - Beta
        #   5 - Production/Stable
        'Development Status :: 3 - Alpha',

        # Indicate who your project is intended for
        'Intended Audience :: Developers',
        'Operating System :: OS Independent',
        'Topic :: Communications :: Ham Radio',
        'Topic :: Software Development :: Libraries :: Python Modules',

        # Pick your license as you wish (should match "license" above)
        'License :: OSI Approved :: Apache Software License',

        # Specify the Python versions you support here. In particular, ensure
        # that you indicate whether you support Python 2, Python 3 or both.
        'Programming Language :: Python :: 3',
    ],
)

print("Installed requirements from: %s" % REQUIRES_FILE)
