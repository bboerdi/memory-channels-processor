#!/usr/bin/env bash

SD=$(cd "$(dirname "$0")"; pwd -P)
WD="$(pwd)"
SCRIPT=$(basename "$0")
SCRIPT_NAME=${SCRIPT%.*}
SCRIPT_EXTENSION=${SCRIPT##*.}
SELF=$SD/$SCRIPT

#/////////////////////////

APP_PYTHON="$(command -v python)"
APP_PIP="$(command -v pip)"
APP_MCP="$(command -v memory-channels-processor)"
APP_GIT="$(command -v git)"

#/////////////////////////

set -eu

#/////////////////////////

if [ ! -e "$APP_PYTHON" ] ; then
    echo "The executable 'python' wasn't found!"
    exit 1
fi

if [ ! -e "$APP_PIP" ] ; then
    echo "The executable 'pip' wasn't found!"
    exit 1
fi

if [ ! -e "$APP_MCP" ] ; then
    echo "The executable 'memory-channels-processor' wasn't found!"
    exit 1
fi

if [ ! -e "$APP_GIT" ] ; then
    echo "The executable 'git' wasn't found!"
    exit 1
fi

#/////////////////////////

cd "$SD/docs/modules/ROOT/examples/" || exit 1

#/////////////////////////

echo "--------------------"

echo "Building examples..."

# Build examples for documentation
$APP_MCP --help > usage.txt

#/////////////////////////

cd "$SD/docs/modules/ROOT/partials/" || exit 1

#/////////////////////////

echo "--------------------"

echo "Building partials..."

# Build partials for documentation
$APP_MCP --asciidoc "data_source_columns" > data_source_columns.adoc
$APP_MCP --asciidoc "data_target_columns" > data_target_columns.adoc
$APP_MCP --asciidoc "name_formats" > name_formats.adoc

#/////////////////////////

cd "$SD/docs/modules/ROOT/attachments/" || exit 1

#/////////////////////////

echo "--------------------"

echo "Building attachments/codeplugs..."

echo "- Build CSV intermediate files..."
$APP_MCP --source "oevsv-repeater-db" --band 70cm --type "fm" --output-file "fm_70cm_gen_oevsv-repeater-db.csv" --output-format="csv" "$@"
$APP_MCP --source "oevsv-repeater-db" --band 2m --type "fm" --output-file "fm_2m_gen_oevsv-repeater-db.csv" --output-format="csv" "$@"
$APP_MCP --source "oevsv-repeater-db" --band 70cm --band 2m --type "fm" --output-file "fm_2m_70cm_gen_oevsv-repeater-db.csv" --output-format="csv" "$@"

$APP_MCP --source "fm-channels-iaru-r1" --band 70cm --type "fm" --output-file "fm_70cm_gen_fm-channels-iaru-r1.csv" --output-format="csv" "$@"
$APP_MCP --source "fm-channels-iaru-r1" --band 2m --type "fm" --output-file "fm_2m_gen_fm-channels-iaru-r1.csv" --output-format="csv" "$@"

$APP_MCP --source "oevsv-repeater-db" --source "fm-channels-iaru-r1" --band 70cm --band 2m --type "fm" --output-file "fm_2m_70cm_gen_oevsv-repeater-db_fm-channels-iaru-r1.csv" --output-format="csv" "$@"

$APP_MCP --source "oevsv-repeater-db" --band 23cm --band 70cm --band 2m --type "d-star" --output-file "d-star_23cm_70cm_2m_gen_oevsv-repeater-db.csv" --output-format="csv" --sort "callsign" --sort "freq_tx" --sort "name" "$@"

$APP_MCP --source "oevsv-repeater-db" --band 70cm --band 2m --type "dmr" --output-file "dmr_70cm_2m_gen_oevsv-repeater-db.csv" --output-format="csv" --sort "callsign" --sort "freq_tx" --sort "name" "$@"


echo "- Build TSV intermediate files..."
$APP_MCP --source "oevsv-repeater-db" --band 70cm --type "fm" --output-file "fm_70cm_gen_oevsv-repeater-db.tsv" --output-format="tsv" "$@"
$APP_MCP --source "oevsv-repeater-db" --band 2m --type "fm" --output-file "fm_2m_gen_oevsv-repeater-db.tsv" --output-format="tsv" "$@"
$APP_MCP --source "oevsv-repeater-db" --band 70cm --band 2m --type "fm" --output-file "fm_2m_70cm_gen_oevsv-repeater-db.tsv" --output-format="tsv" "$@"

$APP_MCP --source "fm-channels-iaru-r1" --band 70cm --type "fm" --output-file "fm_70cm_gen_fm-channels-iaru-r1.tsv" --output-format="tsv" "$@"
$APP_MCP --source "fm-channels-iaru-r1" --band 2m --type "fm" --output-file "fm_2m_gen_fm-channels-iaru-r1.tsv" --output-format="tsv" "$@"

$APP_MCP --source "oevsv-repeater-db" --source "fm-channels-iaru-r1" --band 70cm --band 2m --type "fm" --output-file "fm_2m_70cm_gen_oevsv-repeater-db_fm-channels-iaru-r1.tsv" --output-format="tsv" "$@"

$APP_MCP --source "oevsv-repeater-db" --band 23cm --band 70cm --band 2m --type "d-star" --output-file "d-star_23cm_70cm_2m_gen_oevsv-repeater-db.tsv" --output-format="tsv" --sort "callsign" --sort "freq_tx" --sort "name" "$@"

$APP_MCP --source "oevsv-repeater-db" --band 70cm --band 2m --type "dmr" --output-file "dmr_70cm_2m_gen_oevsv-repeater-db.tsv" --output-format="tsv" --sort "callsign" --sort "freq_tx" --sort "name" "$@"


echo "- Build Icom IC-705/ID-52E files..."
$APP_MCP --source "oevsv-repeater-db" --band 2m --type "fm" --output-file "icom_ic705_id52_fm_2m_gen_oevsv-repeater-db.csv" --output-format="icom-ic-705" --name-format="name" --icom-group-number "00" --icom-group-name "OE 2m Repeaters" "$@"
$APP_MCP --source "oevsv-repeater-db" --source "fm-channels-iaru-r1" --band 2m --type "fm" --output-file "icom_ic705_id52_fm_2m_gen_oevsv-repeater-db_fm-channels-iaru-r1.csv" --output-format="icom-ic-705" --name-format="name" --icom-group-number "00" --icom-group-name "OE 2m Repeaters" "$@"
$APP_MCP --source "oevsv-repeater-db" --band 70cm --band 2m --type "fm" --output-file "icom_ic705_id52_fm_2m_70cm_gen_oevsv-repeater-db.csv" --output-format="icom-ic-705" --icom-group-number "00" --name-format="name" --icom-group-name "OE 2m+70cm Repeaters" "$@"

$APP_MCP --source "fm-channels-iaru-r1" --band 2m --type "fm" --output-file "icom_ic705_id52_fm_2m_gen_fm-channels-iaru-r1.csv" --output-format="icom-ic-705" --name-format="name" --icom-group-number "01" --icom-group-name "OE 2m Channels" "$@"
$APP_MCP --source "fm-channels-iaru-r1" --band 70cm --band 2m --type "fm" --output-file "icom_ic705_id52_fm_2m_70cm_gen_fm-channels-iaru-r1.csv" --output-format="icom-ic-705" --name-format="name" --icom-group-number "01" --icom-group-name "OE 2m+70cm Channels" "$@"

$APP_MCP --source "oevsv-repeater-db" --band 70cm --type "fm" --output-file "icom_ic705_id52_fm_70cm_gen_oevsv-repeater-db.csv" --output-format="icom-ic-705" --name-format="name" --icom-group-number "02" --icom-group-name "OE 70cm Repeaters" "$@"
$APP_MCP --source "oevsv-repeater-db" --source "fm-channels-iaru-r1" --band 70cm --type "fm" --output-file "icom_ic705_id52_fm_70cm_gen_oevsv-repeater-db_fm-channels-iaru-r1.csv" --output-format="icom-ic-705" --name-format="name" --icom-group-number "02" --icom-group-name "OE 70cm Repeaters" "$@"
$APP_MCP --source "oevsv-repeater-db" --source "fm-channels-iaru-r1" --band 70cm --band 2m --type "fm" --output-file "icom_ic705_id52_fm_2m_70cm_gen_oevsv-repeater-db_fm-channels-iaru-r1.csv" --output-format="icom-ic-705" --name-format="name" --icom-group-number "02" --icom-group-name "OE 2m+70cm Repeaters" "$@"

$APP_MCP --source "fm-channels-iaru-r1" --band 70cm --type "fm" --output-file "icom_ic705_id52_fm_70cm_gen_fm-channels-iaru-r1.csv" --output-format="icom-ic-705" --name-format="name" --icom-group-number "03" --icom-group-name "OE 70cm Channels" "$@"

$APP_MCP --source "oevsv-repeater-db" --band 6m --band 10m --type "fm" --output-file "icom_ic705_id52_fm_6m_10m_gen_oevsv-repeater-db.csv" --output-format="icom-ic-705" --name-format="name" --icom-group-number "04" --icom-group-name "OE 6m+10m Repeaters" "$@"

$APP_MCP --source "oevsv-repeater-db" --band 23cm --band 70cm --band 2m --type "d-star" --output-file "icom_ic705_id52_d-star_23cm_70cm_2m_gen_oevsv-repeater-db.csv" --output-format="icom-ic-705" --name-format="name" --icom-group-number "1" --icom-group-name "Austria" --icom-type="d-star" --sort "callsign" --sort "freq_tx" --sort "name" "$@"


echo "- Build Icom RS-MS1 files..."
$APP_MCP --source "oevsv-repeater-db" --band 23cm --band 70cm --band 2m --type "d-star" --output-file "icom_rsms1_d-star_23cm_70cm_2m_gen_oevsv-repeater-db.csv" --output-format="icom-rs-ms1" --name-format="name" --icom-group-number "22" --icom-group-name "Austria" --icom-type="d-star" --sort "callsign" --sort "freq_tx" --sort "name" "$@"


echo "- Build Icom IC-9700 files..."
$APP_MCP --source "oevsv-repeater-db" --band 2m --type "fm" --output-file "icom_ic9700_fm_2m_gen_oevsv-repeater-db.csv" --output-format="icom-ic-9700" --name-format="name" --icom-group-number "00" --icom-group-name "OE 2m Repeaters" "$@"
$APP_MCP --source "oevsv-repeater-db" --source "fm-channels-iaru-r1" --band 2m --type "fm" --output-file "icom_ic9700_fm_2m_gen_oevsv-repeater-db_fm-channels-iaru-r1.csv" --output-format="icom-ic-9700" --name-format="name" --icom-group-number "00" --icom-group-name "OE 2m Repeaters" "$@"

$APP_MCP --source "fm-channels-iaru-r1" --band 2m --type "fm" --output-file "icom_ic9700_fm_2m_gen_fm-channels-iaru-r1.csv" --output-format="icom-ic-9700" --name-format="name" --icom-group-number "01" --icom-group-name "OE 2m Channels" "$@"

$APP_MCP --source "oevsv-repeater-db" --band 70cm --type "fm" --output-file "icom_ic9700_fm_70cm_gen_oevsv-repeater-db.csv" --output-format="icom-ic-9700" --name-format="name" --icom-group-number "02" --icom-group-name "OE 70cm Repeaters" "$@"
$APP_MCP --source "oevsv-repeater-db" --source "fm-channels-iaru-r1" --band 70cm --type "fm" --output-file "icom_ic9700_fm_70cm_gen_oevsv-repeater-db_fm-channels-iaru-r1.csv" --output-format="icom-ic-9700" --name-format="name" --icom-group-number "02" --icom-group-name "OE 70cm Repeaters" "$@"

$APP_MCP --source "fm-channels-iaru-r1" --band 70cm --type "fm" --output-file "icom_ic9700_fm_70cm_gen_fm-channels-iaru-r1.csv" --output-format="icom-ic-9700" --name-format="name" --icom-group-number "03" --icom-group-name "OE 70cm Channels" "$@"

$APP_MCP --source "oevsv-repeater-db" --band 23cm --type "fm" --output-file "icom_ic9700_fm_23cm_gen_oevsv-repeater-db.csv" --output-format="icom-ic-9700" --name-format="name" --icom-group-number "04" --icom-group-name "OE 23cm Repeaters" "$@"

$APP_MCP --source "oevsv-repeater-db" --band 23cm --band 70cm --band 2m --type "d-star" --output-file "icom_ic9700_d-star_23cm_70cm_2m_gen_oevsv-repeater-db.csv" --output-format="icom-ic-9700" --name-format="name" --icom-group-number "1" --icom-group-name "Austria" --icom-type="d-star" --sort "callsign" --sort "freq_tx" --sort "name" "$@"


echo "- Build Chirp files..."
$APP_MCP --source "oevsv-repeater-db" --band 70cm --type "fm" --output-file "chirp_fm_70cm_gen_oevsv-repeater-db_callsign.csv" --output-format="chirp" --name-format "callsign" "$@"
$APP_MCP --source "oevsv-repeater-db" --band 70cm --type "fm" --output-file "chirp_fm_70cm_gen_oevsv-repeater-db_callsign-5-char.csv" --output-format="chirp" --name-format "5-char" "$@"
$APP_MCP --source "oevsv-repeater-db" --band 70cm --type "fm" --output-file "chirp_fm_70cm_gen_oevsv-repeater-db_callsign-6-char.csv" --output-format="chirp" --name-format "6-char" "$@"
$APP_MCP --source "oevsv-repeater-db" --band 70cm --type "fm" --output-file "chirp_fm_70cm_gen_oevsv-repeater-db_callsign-7-char.csv" --output-format="chirp" --name-format "7-char" "$@"
$APP_MCP --source "oevsv-repeater-db" --band 70cm --type "fm" --output-file "chirp_fm_70cm_gen_oevsv-repeater-db_callsign-name.csv" --output-format="chirp" --name-format "callsign-name" "$@"
$APP_MCP --source "oevsv-repeater-db" --band 70cm --type "fm" --output-file "chirp_fm_70cm_gen_oevsv-repeater-db_callsign-mode-name.csv" --output-format="chirp" --name-format "callsign-mode-name" "$@"
$APP_MCP --source "oevsv-repeater-db" --band 70cm --type "fm" --output-file "chirp_fm_70cm_gen_oevsv-repeater-db_name.csv" --output-format="chirp" --name-format "name" "$@"
$APP_MCP --source "oevsv-repeater-db" --band 70cm --type "fm" --output-file "chirp_fm_70cm_gen_oevsv-repeater-db_name-5-char.csv" --output-format="chirp" --name-format "name-5-char" "$@"
$APP_MCP --source "oevsv-repeater-db" --band 70cm --type "fm" --output-file "chirp_fm_70cm_gen_oevsv-repeater-db_name-6-char.csv" --output-format="chirp" --name-format "name-6-char" "$@"
$APP_MCP --source "oevsv-repeater-db" --band 70cm --type "fm" --output-file "chirp_fm_70cm_gen_oevsv-repeater-db_name-7-char.csv" --output-format="chirp" --name-format "name-7-char" "$@"
$APP_MCP --source "oevsv-repeater-db" --band 70cm --type "fm" --output-file "chirp_fm_70cm_gen_oevsv-repeater-db_name-callsign.csv" --output-format="chirp" --name-format "name-callsign" "$@"
$APP_MCP --source "oevsv-repeater-db" --band 70cm --type "fm" --output-file "chirp_fm_70cm_gen_oevsv-repeater-db_name-mode-callsign.csv" --output-format="chirp" --name-format "name-mode-callsign" "$@"

$APP_MCP --source "oevsv-repeater-db" --band 2m --type "fm" --output-file "chirp_fm_2m_gen_oevsv-repeater-db_callsign.csv" --output-format="chirp" --name-format "callsign" "$@"
$APP_MCP --source "oevsv-repeater-db" --band 2m --type "fm" --output-file "chirp_fm_2m_gen_oevsv-repeater-db_callsign-5-char.csv" --output-format="chirp" --name-format "5-char" "$@"
$APP_MCP --source "oevsv-repeater-db" --band 2m --type "fm" --output-file "chirp_fm_2m_gen_oevsv-repeater-db_callsign-6-char.csv" --output-format="chirp" --name-format "6-char" "$@"
$APP_MCP --source "oevsv-repeater-db" --band 2m --type "fm" --output-file "chirp_fm_2m_gen_oevsv-repeater-db_callsign-7-char.csv" --output-format="chirp" --name-format "7-char" "$@"
$APP_MCP --source "oevsv-repeater-db" --band 2m --type "fm" --output-file "chirp_fm_2m_gen_oevsv-repeater-db_callsign-name.csv" --output-format="chirp" --name-format "callsign-name" "$@"
$APP_MCP --source "oevsv-repeater-db" --band 2m --type "fm" --output-file "chirp_fm_2m_gen_oevsv-repeater-db_callsign-mode-name.csv" --output-format="chirp" --name-format "callsign-mode-name" "$@"
$APP_MCP --source "oevsv-repeater-db" --band 2m --type "fm" --output-file "chirp_fm_2m_gen_oevsv-repeater-db_name.csv" --output-format="chirp" --name-format "name" "$@"
$APP_MCP --source "oevsv-repeater-db" --band 2m --type "fm" --output-file "chirp_fm_2m_gen_oevsv-repeater-db_name-5-char.csv" --output-format="chirp" --name-format "name-5-char" "$@"
$APP_MCP --source "oevsv-repeater-db" --band 2m --type "fm" --output-file "chirp_fm_2m_gen_oevsv-repeater-db_name-6-char.csv" --output-format="chirp" --name-format "name-6-char" "$@"
$APP_MCP --source "oevsv-repeater-db" --band 2m --type "fm" --output-file "chirp_fm_2m_gen_oevsv-repeater-db_name-7-char.csv" --output-format="chirp" --name-format "name-7-char" "$@"
$APP_MCP --source "oevsv-repeater-db" --band 2m --type "fm" --output-file "chirp_fm_2m_gen_oevsv-repeater-db_name-callsign.csv" --output-format="chirp" --name-format "name-callsign" "$@"
$APP_MCP --source "oevsv-repeater-db" --band 2m --type "fm" --output-file "chirp_fm_2m_gen_oevsv-repeater-db_name-mode-callsign.csv" --output-format="chirp" --name-format "name-mode-callsign" "$@"


echo "- Build OpenGD77 files..."
$APP_MCP --source "oevsv-repeater-db" --type "fm" --type "dmr" --output-file "opengd77_fm+dmr_gen_oevsv-repeater-db_callsign.csv" --output-format="opengd77" --name-format "callsign" "$@"
$APP_MCP --source "oevsv-repeater-db" --type "fm" --type "dmr" --output-file "opengd77_fm+dmr_gen_oevsv-repeater-db_callsign-5-char.csv" --output-format="opengd77" --name-format "5-char" "$@"
$APP_MCP --source "oevsv-repeater-db" --type "fm" --type "dmr" --output-file "opengd77_fm+dmr_gen_oevsv-repeater-db_callsign-6-char.csv" --output-format="opengd77" --name-format "6-char" "$@"
$APP_MCP --source "oevsv-repeater-db" --type "fm" --type "dmr" --output-file "opengd77_fm+dmr_gen_oevsv-repeater-db_callsign-7-char.csv" --output-format="opengd77" --name-format "7-char" "$@"
$APP_MCP --source "oevsv-repeater-db" --type "fm" --type "dmr" --output-file "opengd77_fm+dmr_gen_oevsv-repeater-db_callsign-name.csv" --output-format="opengd77" --name-format "callsign-name" "$@"
$APP_MCP --source "oevsv-repeater-db" --type "fm" --type "dmr" --output-file "opengd77_fm+dmr_gen_oevsv-repeater-db_callsign-mode-name.csv" --output-format="opengd77" --name-format "callsign-mode-name" "$@"
$APP_MCP --source "oevsv-repeater-db" --type "fm" --type "dmr" --output-file "opengd77_fm+dmr_gen_oevsv-repeater-db_name.csv" --output-format="opengd77" --name-format "name" "$@"
$APP_MCP --source "oevsv-repeater-db" --type "fm" --type "dmr" --output-file "opengd77_fm+dmr_gen_oevsv-repeater-db_name-5-char.csv" --output-format="opengd77" --name-format "name-5-char" "$@"
$APP_MCP --source "oevsv-repeater-db" --type "fm" --type "dmr" --output-file "opengd77_fm+dmr_gen_oevsv-repeater-db_name-6-char.csv" --output-format="opengd77" --name-format "name-6-char" "$@"
$APP_MCP --source "oevsv-repeater-db" --type "fm" --type "dmr" --output-file "opengd77_fm+dmr_gen_oevsv-repeater-db_name-7-char.csv" --output-format="opengd77" --name-format "name-7-char" "$@"
$APP_MCP --source "oevsv-repeater-db" --type "fm" --type "dmr" --output-file "opengd77_fm+dmr_gen_oevsv-repeater-db_name-callsign.csv" --output-format="opengd77" --name-format "name-callsign" "$@"
$APP_MCP --source "oevsv-repeater-db" --type "fm" --type "dmr" --output-file "opengd77_fm+dmr_gen_oevsv-repeater-db_name-mode-callsign.csv" --output-format="opengd77" --name-format "name-mode-callsign" "$@"

echo "--------------------"

#/////////////////////////

cd "$SD/" || exit 1

#/////////////////////////

# Check if running in GitLabCI
if [ -n "${GITLAB_CI:-}" ] && [ -n "${GITLAB_CI_REPO_TOKEN:-}" ] ; then
  echo "Add generated codeplugs to git repo..."

  # Set user information (if present)
  if [ -n "${GITLAB_USER_EMAIL:-}" ] ; then
    $APP_GIT config user.email "$GITLAB_USER_EMAIL"
  fi
  if [ -n "${GITLAB_USER_NAME:-}" ] ; then
    $APP_GIT config user.name "$GITLAB_USER_NAME"
  fi

  # Add generated files
  $APP_GIT add -v "docs/modules/ROOT/examples/usage.txt"
  $APP_GIT add -v "docs/modules/ROOT/partials/data_source_columns.adoc"
  $APP_GIT add -v "docs/modules/ROOT/partials/data_target_columns.adoc"
  $APP_GIT add -v "docs/modules/ROOT/partials/name_formats.adoc"
  $APP_GIT add -v "docs/modules/ROOT/attachments/*.csv"
  $APP_GIT add -v "docs/modules/ROOT/attachments/*.tsv"

  # Check if commit and push is needed
  GIT_STATUS=$($APP_GIT status --porcelain --untracked-files=no)
  if [ -n "$GIT_STATUS" ]; then
    echo "Commit changed files..."
    $APP_GIT status
    $APP_GIT commit -m "CI: Update generated files"

    # Set appropriate remote for repo and push changes there
    $APP_GIT remote remove gitlab_ci_origin || true
    $APP_GIT remote add gitlab_ci_origin "https://gitlab-ci-token:${GITLAB_CI_REPO_TOKEN}@${CI_SERVER_HOST}:${CI_SERVER_PORT}/${CI_PROJECT_PATH}.git"
    $APP_GIT remote show gitlab_ci_origin
    $APP_GIT push gitlab_ci_origin "HEAD:$CI_COMMIT_REF_NAME" -o ci.skip # Prevent triggering the pipeline again

    # Retrieve the actual commit hash and store it for the next build step
    GIT_COMMIT_SHA=$($APP_GIT rev-parse --verify HEAD)
    echo "CODEPLUGS_GIT_COMMIT_SHA=$GIT_COMMIT_SHA" >> "$SD/build.env"
  else
    echo "No files to commit"
    echo "CODEPLUGS_GIT_COMMIT_SHA=" >> "$SD/build.env"
  fi

  echo "--------------------"
else
  echo "CODEPLUGS_GIT_COMMIT_SHA=" >> "$SD/build.env"
fi

echo "Done"
echo "--------------------"

#/////////////////////////

cd "$WD/" || exit 1

#/////////////////////////