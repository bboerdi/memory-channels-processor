@ECHO OFF

SET WD=%CD%
SET SD=%~dp0
SET PARAMS=%*

cd "%SD%"

call git update-index --chmod=+x *.sh
call git ls-tree HEAD

cd "%WD%"
