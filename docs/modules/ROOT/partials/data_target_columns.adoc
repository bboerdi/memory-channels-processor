[%header,%autowidth,stripes=even,format=dsv,separator=;]
|===
Key;Type
# tag::band[]
band;object
# end::band[]
# tag::callsign[]
callsign;object
# end::callsign[]
# tag::country[]
country;object
# end::country[]
# tag::ctcss[]
ctcss;bool
# end::ctcss[]
# tag::ctcss_rx[]
ctcss_rx;float64
# end::ctcss_rx[]
# tag::ctcss_tx[]
ctcss_tx;float64
# end::ctcss_tx[]
# tag::distance[]
distance;float64
# end::distance[]
# tag::dmr[]
dmr;bool
# end::dmr[]
# tag::dmr_id[]
dmr_id;int64
# end::dmr_id[]
# tag::dstar[]
dstar;bool
# end::dstar[]
# tag::dstar_rpt1[]
dstar_rpt1;object
# end::dstar_rpt1[]
# tag::dstar_rpt2[]
dstar_rpt2;object
# end::dstar_rpt2[]
# tag::dup[]
dup;object
# end::dup[]
# tag::fm[]
fm;bool
# end::fm[]
# tag::freq_rx[]
freq_rx;float64
# end::freq_rx[]
# tag::freq_tx[]
freq_tx;float64
# end::freq_tx[]
# tag::heading[]
heading;int64
# end::heading[]
# tag::landmark[]
landmark;object
# end::landmark[]
# tag::lat[]
lat;float64
# end::lat[]
# tag::loc_exact[]
loc_exact;bool
# end::loc_exact[]
# tag::locator[]
locator;object
# end::locator[]
# tag::long[]
long;float64
# end::long[]
# tag::name[]
name;object
# end::name[]
# tag::name_formatted[]
name_formatted;object
# end::name_formatted[]
# tag::offset[]
offset;float64
# end::offset[]
# tag::state[]
state;object
# end::state[]
|===
