= Memory Channels Processor
:navtitle: Home

[NOTE]
====
Bitte beachten Sie, dass es sich bei diesem Dokument um einen Entwurf handelt, der noch in Arbeit ist.
Es wird der Öffentlichkeit nur zu Informationszwecken zur Verfügung gestellt. Da es sich um ein unfertiges Dokument handelt, fehlen einige Teile oder werden gerade überarbeitet.
====


Das Programm `Memory Channels Processor` bietet die Möglichkeit Simplex- und Relais-Kanäle aus verschiedenen Quellen für verschiedene Geräte (z.B. Icom IC-705, ID-52E oder IC-9700) oder für Software (z.B. Chirp, OpenGD77) zur Weiterverarbeitung aufzubereiten.

Die ursprüngliche Umsetzungfootnote:[https://codeberg.org/OE4DNS/IC-705-Memory-Channels-Austria/] wurde mit dem Ziel entwickelt die österreichischen 2m/70cm FM und D-STAR Amateurfunk Relaiskanälefootnote:[https://www.oevsv.at/funkbetrieb/amateurfunkfrequenzen/ukw-referat/] möglichst automatisiert für den Import für das Icom IC-705 und ID-52E bereitzustellen.

Die zugrundeliegende Idee wurde in dieser Applikation weiterentwickelt und auf andere technische Beine gestellt.

Mittlerweile gibt es auch die Möglichkeit Daten von anderen Quellen zu importieren und Daten dieser Quellen zu mischen und auch eigens definierte Datensätze hinzuzufügen.

[plantuml, target=mcp_overview, format=svg]
....

' -- Params

left to right direction

skinparam BackgroundColor transparent
skinparam componentStyle rectangle


' -- Components and packages

cloud source_oevsv_repeater_db as "ÖVSV Repeater DB"
cloud source_ha2to_repeater_list as "HA2TO Repeater List"
cloud source_iaru_simplex_channels as "Simplex Kanäle"
file source_csv as "CSV"
file source_tsv as "TSV"

file target_icom_file as "Icom CSV Format"
agent target_icom_CS_705 as "Icom CS-705"
agent target_icom_CS_52 as "Icom CS-52"
agent target_icom_CS_9700 as "Icom CS-9700"
agent target_icom_RS_BA1 as "Icom RS-BA1v2"
agent target_icom_RS_MS1 as "Icom RS-MS1A"
file target_chirp_file as "Chirp CSV Format"
file target_opengd77_file as "OpenGD77 CSV Channels"
agent target_chirp as "Chirp next"
agent target_opengd77 as "OpenGD77 CPS"
file target_csv_file as "CSV"
file target_tsv_file as "TSV"
file target_xlsx_file as "Excel"

package "Memory Channels Processor" {
    component mcp_read as "Einlesen"
    component mcp_filter as "Filtern"
    component mcp_sort as "Sortieren"
    component mcp_transform as "Transformieren"
    component mcp_output as "Ausgabe"
}


' -- Relations

source_oevsv_repeater_db --> mcp_read
    source_oevsv_repeater_db -right[hidden]- source_ha2to_repeater_list
source_ha2to_repeater_list --> mcp_read
    source_ha2to_repeater_list -right[hidden]- source_iaru_simplex_channels
source_iaru_simplex_channels --> mcp_read
    source_iaru_simplex_channels -right[hidden]- source_csv
source_csv --> mcp_read
    source_csv -right[hidden]- source_tsv
source_tsv --> mcp_read

mcp_read -down-> mcp_output
mcp_read -left[#gray,dotted]-> mcp_filter
mcp_filter -down[#gray,dotted]-> mcp_sort
mcp_sort -right[#gray,dotted]-> mcp_transform
mcp_transform -right[#gray,dotted]-> mcp_output

mcp_output --> target_chirp_file
target_chirp_file -[#gray,dotted]-> target_chirp
    target_chirp_file -right[hidden]- target_opengd77_file
mcp_output --> target_opengd77_file
target_opengd77_file -[#gray,dotted]-> target_opengd77
    target_opengd77_file -right[hidden]- target_icom_file
mcp_output --> target_icom_file
target_icom_file -[#gray,dotted]-> target_icom_CS_705
target_icom_file -[#gray,dotted]-> target_icom_CS_52
target_icom_file -[#gray,dotted]-> target_icom_CS_9700
target_icom_file -[#gray,dotted]-> target_icom_RS_BA1
target_icom_file -[#gray,dotted]-> target_icom_RS_MS1
    target_icom_file -right[hidden]- target_csv_file
mcp_output --> target_csv_file
    target_csv_file -right[hidden]- target_tsv_file
mcp_output --> target_tsv_file
    target_tsv_file -right[hidden]- target_xlsx_file
mcp_output --> target_xlsx_file


' -- Params

remove @unlinked
....


*Setup:*

* xref:setup.adoc[]


*Import Möglichkeiten:*

* link:https://www.oevsv.at/funkbetrieb/amateurfunkfrequenzen/ukw-referat/[ÖVSV-Repeater Datenbank,{linkExternal}]
* link:http://ha2to.orbel.hu/content/repeaters/en/index.html[HA2TO Repeater Liste,{linkExternal}]
* xref:export_csv.adoc[]
* xref:export_tsv.adoc[]


*Export Möglichkeiten:*

* xref:codeplug_chirp.adoc[]
* xref:codeplug_icom.adoc[]
* xref:codeplug_opengd77.adoc[]
* xref:export_csv.adoc[]
* xref:export_tsv.adoc[]
* xref:export_xlsx.adoc[]


[NOTE]
====
Abhängig von den Spracheinstellungen des Rechners, auf dem der Import der Codeplugs vorgenommen werden soll, kann es vorkommen, dass manche Codeplugs davon abweichende Zeichen als Dezimalzeichen verwenden.

In diesem Fall können die Dezimalzeichen in den Codeplugs z.B. mit einem Texteditor angepasst werden.
====


== Downloads

Für einige Geräte und Formate gibt es automatisch generierte Codeplugs die zum Download angeboten werden.
Diese Dateien sind unter anderem auf den folgenden Seiten zu finden:

* xref:codeplug_chirp.adoc[]
* xref:codeplug_icom.adoc[]
* xref:codeplug_opengd77.adoc[]


== Bedienung

Die Bedienung des `Memory Channels Processor` erfolgt über die Kommandozeile.
Folgende Optionen und Parameter stehen zur Auswahl:

[source,bash]
----
$ memory-channels-processor --help
include::example$usage.txt[tag=**]
----


== Softwareentwicklung

Details zur Entwicklung und zum Development-Setup findet man auf den folgenden Seiten:

* xref:development.adoc[]
