import pytest

# we reuse a bit of pytest's own testing machinery, this should eventually come
# from a separately installable pytest-cli plugin.
pytest_plugins = ["pytester"]


@pytest.fixture
def run(testdir):
    def do_run(*args):
        args = ["memory-channels-processor"] + list(args)
        return testdir.run(*args)

    return do_run


def test_version(tmpdir, run):
    result = run("--version")
    assert result.ret == 0
