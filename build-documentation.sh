#!/usr/bin/env bash

SD=$(cd "$(dirname "$0")"; pwd -P)
WD="$(pwd)"
SCRIPT=$(basename "$0")
SCRIPT_NAME=${SCRIPT%.*}
SCRIPT_EXTENSION=${SCRIPT##*.}
SELF=$SD/$SCRIPT

#/////////////////////////

APP_NPM="$(command -v npm)"
APP_NPX="$(command -v npx)"

#/////////////////////////

set -eu

#/////////////////////////

if [ ! -e "$APP_NPM" ] ; then
    echo "The executable 'npm' wasn't found!"
    exit
fi

if [ ! -e "$APP_NPX" ] ; then
    echo "The executable 'npx' wasn't found!"
    exit
fi

#/////////////////////////

cd "$SD/" || exit 1

#/////////////////////////

echo "--------------------"

# Install dependencies defined in 'package.json'
echo "Install dependencies defined in 'package.json'"
$APP_NPM ci
echo "--------------------"

# Run Antora
echo "Run Antora"
$APP_NPX antora --fetch --cache-dir "$SD/.cache/antora" --redirect-facility=gitlab --log-failure-level=error --log-level=all --to-dir="$SD/public" --stacktrace --attribute generated_timestamp="$(date -u)" antora-playbook.yml $@
echo "--------------------"

#/////////////////////////

cd "$WD/" || exit 1

#/////////////////////////